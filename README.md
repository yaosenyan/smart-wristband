# 智能手环

#### 介绍
基于STM32F411CEU6
基本功能：
①指示灯和振动(LED灯和电机)
②按键功能  (垂直按键,五方向按键ADC)
③显示      (OLED屏幕)
④显示时间  (RTC) 
⑤温湿度测量(SHT20)
⑥血压心率  (HP6)
⑦计步      (MPU6050)

#### 软件架构
软件架构说明


STM32F411CEU6芯片介绍
	内核	：ARM-----CM4
	时钟主频：100MHZ    (APB1 50MHZ)  (APB2 100MHZ) 
	管脚    ：48
	IO      ：2组+(GPIOA  GPIOB)    32+个
	flash    ：512K
	SRAM	：128K  
	TIMER	：8个    
	RTC 	：1个
	ADC		：1个
	USART	：3个
	DMA	：2个
STM32F4407VGT6芯片介绍
	内核    ：ARM-----CM4
	时钟主频：168MHZ    (APB1 42MHZ)  (APB2 84MHZ)  
	管脚    ：100
	IO      ：5组 （GPIOA----GPIOE）   80个
	flash   	：1024K     512K
	SRAM   ：192K
	TIMER  ：14个
	RTC		：1个
	ADC		：3个
	USART	：6个
	DMA	：2个
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
