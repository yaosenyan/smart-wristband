#include "key.h"
#include "adc.h"
#include "timer.h"

static void delay_ms(u32 ms)
{
	u32 i=168/4*1000*ms;
	while(i)
	{
		i--;
	}
}


void Key_Init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);                     //rcc.h里面找    //没有手敲代码
	
	//GPIO控制器配置
	GPIO_InitTypeDef GPIO_InitStruct = {0};       //定义gpio配置结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;    //选择通用输出模式
 	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0;        //0号io口
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL; //无上下拉
  GPIO_Init(GPIOA,&GPIO_InitStruct);            
}

u8 Key_Scan(void)
{
	static u8 flag=1;
	u8 key=0;
	if(KEY1 && (flag==1))
	{
		Tim11_Delay_Ms(10);
		if(KEY1)
		{
			key=KEY_OK;
			flag=0;
		}
	}
	if(!KEY1)
	{
		flag=1;
	}
	
	return key; //返回键值
}

/***********************************************
*函数名    :Fun_Key_Scan
*函数功能  :五方向按键
*函数参数  :无
*函数返回值:u8
*函数描述  :
************************************************/

u8 Fun_Key_Scan(void)
{
	u16 data;
	u8 val=0xff;
	static u8 adc_flag=1;     //标志位
	
	val = Key_Scan();      //垂直按键扫描函数
	
	
	data = Get_Adc1_Data();//adc转换值 --- 四个方向转换值不一样
	if(data>=1000 && data<=1100 && adc_flag)  //up
	{
		adc_flag=0;
		val=KEY_UP;
	}
	else if(data>=1300 && data<=1400 && adc_flag)  //dowm
	{
	  adc_flag=0;	
		val=KEY_DOWN;
	}
	else if(data>4000 && adc_flag)  //left
	{
	  adc_flag=0;	
		val=KEY_LEFT;
	}
	else if(data>=2000 && data<=2100 && adc_flag)  //right
	{
	  adc_flag=0;	
		val=KEY_RIGHT;
	}
	if(data<=20)    //解锁标志位 
	{
		adc_flag=1;
	}
	return val;
}
	


