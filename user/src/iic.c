#include "iic.h"
#include "timer.h"


void IIC_W_SCL(u8 BitValue)
{
	GPIO_WriteBit(GPIOB,GPIO_Pin_8,(BitAction)BitValue);
	Tim11_Delay_Us(10);
}

void IIC_W_SDA(u8 BitValue)
{
	GPIO_WriteBit(GPIOB,GPIO_Pin_9,(BitAction)BitValue);
	Tim11_Delay_Us(10);
}

u8 IIC_R_SDA(void)
{
	u8 BitValue;
	BitValue = GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_9);
	Tim11_Delay_Us(10);
	return BitValue;
}

void IIC_Start(void)
{
	IIC_W_SDA(1);  //为了能拉低电平
	IIC_W_SCL(1);
	IIC_W_SDA(0);
	IIC_W_SCL(0);  //为了能数据准备
}

void IIC_Stop(void)
{
	IIC_W_SDA(0);  //为了能拉高电平
	IIC_W_SCL(1);
	IIC_W_SDA(1);
	
}

void IIC_SendByte(u8 Byte)
{
	u8 i;
	for(i = 0;i < 8;i++)
	{
		IIC_W_SDA(Byte & (0x80 >> i));
		IIC_W_SCL(1);
		IIC_W_SCL(0);
	}
}

u8 IIC_ReceiveByte(void)
{
	u8 i,Byte = 0x00;
	IIC_W_SDA(1);  //主机先释放SDA
	for(i = 0;i < 8;i++)
	{
		IIC_W_SCL(1);
		if(IIC_R_SDA() == 1){Byte |= (0x80 >> i);}
		IIC_W_SCL(0);
	}
	return Byte;
}

void IIC_SendAck(u8 AckBit)
{
	IIC_W_SDA(AckBit);
	IIC_W_SCL(1);
	IIC_W_SCL(0);
}

u8 IIC_ReceiveAck(void)
{
	u8 AckBit; 
	IIC_W_SDA(1);
	IIC_W_SCL(1);
	AckBit = IIC_R_SDA();
	IIC_W_SCL(0);
	return AckBit;
}
/***********************************************
*函数名    :iic_io_init     sht20
*函数功能  :IIC所用IO口初始化
*函数参数  :无
*函数返回值:无
*函数描述  :IIC_SCL----------PB8  原理图   通用推挽输出
            IIC_SDA----------PB9           通用开漏输出
************************************************/
void IIC_IO_Init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);                     //rcc.h里面找    //没有手敲代码
	
	//GPIO控制器配置
	GPIO_InitTypeDef GPIO_InitStruct = {0};       //定义gpio配置结构体变量
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;    //选择开漏模式
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
 	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;        //8、9号io口
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; 
  GPIO_Init(GPIOB,&GPIO_InitStruct);  

	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;  //或开漏也行
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8; 
	GPIO_Init(GPIOB,&GPIO_InitStruct); 
	
	GPIO_SetBits(GPIOB,GPIO_Pin_8 | GPIO_Pin_9);
}



/*****************************************************************************
																HP6_IIC
*****************************************************************************/

//void HP6_IIC_W_SCL(u8 BitValue)
//{
//	GPIO_WriteBit(GPIOA,GPIO_Pin_2,(BitAction)BitValue);
//	Tim11_Delay_Us(10);
//}

//void HP6_IIC_W_SDA(u8 BitValue)
//{
//	GPIO_WriteBit(GPIOA,GPIO_Pin_1,(BitAction)BitValue);
//	Tim11_Delay_Us(10);
//}

////读数据线
//u8 HP6_IIC_R_SDA(void)
//{
//	u8 value;
//	value = GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_1);
//	Tim11_Delay_Us(10);
//	return value;
//}

///***********************************************
//*函数名    :HP6_IIC_Star
//*函数功能  :IIC的起始信号函数
//*函数参数  :无
//*函数返回值:无
//*函数描述  :
//************************************************/
//void HP6_IIC_Star(void)
//{
////	//拉低时钟线
////	HP6_IIC_W_SCL(0);
//	//拉高数据线，为了产生下降沿
//	HP6_IIC_W_SDA(1);
//	HP6_IIC_W_SCL(1);
//	HP6_IIC_W_SDA(0);
//	HP6_IIC_W_SCL(0);
//}

///***********************************************
//*函数名    :HP6_IIC_Stop
//*函数功能  :IIC的停止信号函数
//*函数参数  :无
//*函数返回值:无
//*函数描述  :
//************************************************/
//void HP6_IIC_Stop(void)
//{
////	//拉低时钟线
////	HP6_IIC_W_SCL(0);
//	//拉低数据线，为了产生上降沿
//	HP6_IIC_W_SDA(0);
//	HP6_IIC_W_SCL(1);
//	HP6_IIC_W_SDA(1);
//}

///***********************************************
//*函数名    :HP6_IIC_SendAck
//*函数功能  :IIC发送应答/不应答函数
//*函数参数  :u8 ack
//*函数返回值:无
//*函数描述  :ack:  0   应答
//           ack:  1   不应答
//************************************************/
//void HP6_IIC_SendAck(u8 AckBit)
//{
////	HP6_IIC_W_SCL(0);
//	HP6_IIC_W_SDA(AckBit);
//	HP6_IIC_W_SCL(1);
//	HP6_IIC_W_SCL(0);
//}

///***********************************************
//*函数名    :HP6_IIC_ReceiveAck
//*函数功能  :IIC返回应答/不应答函数
//*函数参数  :无
//*函数返回值:u8
//*函数描述  :返回0   应答
//           返回1   不应答
//************************************************/
//u8 HP6_IIC_ReceiveAck(void) 
//{
//	u8 value;
//	//释放数据线
//	HP6_IIC_W_SDA(1);
//	HP6_IIC_W_SCL(1);
//	value = HP6_IIC_R_SDA();
//	HP6_IIC_W_SCL(0);
//	return value;
//}

///***********************************************
//*函数名    :HP6_IIC_SendByte
//*函数功能  :IIC发送一个字节函数
//*函数参数  :u8
//*函数返回值:无
//*函数描述  :
//************************************************/
//void HP6_IIC_SendByte(u8 Byte)
//{
//	for(u8 i = 0;i < 8;i++)
//	{
////		HP6_IIC_W_SCL(0);
//		HP6_IIC_W_SDA(Byte & (0x80 >> i));
//		HP6_IIC_W_SCL(1);
//		HP6_IIC_W_SCL(0);		
//	}
//}


///***********************************************
//*函数名    :HP6_IIC_ReceiveByte
//*函数功能  :IIC接收一个字节函数
//*函数参数  :无
//*函数返回值:u8 
//*函数描述  :
//************************************************/
//u8 HP6_IIC_ReceiveByte(void)
//{
//	u8 value = 0x00;
//	HP6_IIC_W_SDA(1);
//	for(u8 i = 0;i < 8;i++)
//	{
////		HP6_IIC_W_SCL(0);	
//		HP6_IIC_W_SCL(1);
//		if(HP6_IIC_R_SDA() == 1){value |= 0x08 >> i;}
//		HP6_IIC_W_SCL(0);
//	}
//	return value;
//}


//void HP6_IIC_IO_Init(void)
//{
//	//打开时钟
//	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);                     //rcc.h里面找    //没有手敲代码
//	
//	//GPIO控制器配置
//	GPIO_InitTypeDef GPIO_InitStruct = {0};       //定义gpio配置结构体变量
//	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;    //选择开漏模式
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
// 	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;        //1号io口
//	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
//	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; 
//  GPIO_Init(GPIOA,&GPIO_InitStruct);  

//	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;  //或开漏也行
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2; 
//	GPIO_Init(GPIOA,&GPIO_InitStruct); 
//	
//	GPIO_SetBits(GPIOA,GPIO_Pin_1 | GPIO_Pin_2);
//}








///***********************************************
//*函数名    :HP6_IIC_Star
//*函数功能  :IIC的起始信号函数
//*函数参数  :无
//*函数返回值:无
//*函数描述  :
//************************************************/
//void HP6_IIC_Star(void)
//{
////拉低时钟线      //为了可以改变数据线
//	HP6_IIC_SCL_L;
//	
//  //拉高数据线      //为了产生下降沿
//	HP6_IIC_SDA_OUT_H;
//	
//	//拉高钟据线      //产生起始信号的条件
//	HP6_IIC_SCL_H;
//	
//	//延时5us
//	Tim11_Delay_Us(10);
//	
//	//拉低数据线      //产生下降沿从而产生起始信号
//	HP6_IIC_SDA_OUT_L;
//	
//	//延时5us
//	Tim11_Delay_Us(10);
//	
//	//拉低时钟线      //位了避免时钟在高电平状态下，数据线改变而出现起始信号或者停止信号
//	HP6_IIC_SCL_L;
//}

///***********************************************
//*函数名    :HP6_IIC_Stop
//*函数功能  :IIC的停止信号函数
//*函数参数  :无
//*函数返回值:无
//*函数描述  :
//************************************************/
//void HP6_IIC_Stop(void)
//{
//	//拉低时钟线
//	HP6_IIC_SCL_L;       //为了可以动数据线
//	
//	//拉低数据线
//	HP6_IIC_SDA_OUT_L;   //为了可以产生上升沿
//	
//	//拉高时钟线
//	HP6_IIC_SCL_H;       //产生停止信号条件
//	
//	//延时5us
//	Tim11_Delay_Us(10);
//	
//	//拉高数据线
//	HP6_IIC_SDA_OUT_H;   //产生上升沿从而产生停止信号
//	
//	//延时5us
//	Tim11_Delay_Us(10);
//}

///***********************************************
//*函数名    :HP6_IIC_SendAck
//*函数功能  :IIC发送应答/不应答函数
//*函数参数  :u8 ack
//*函数返回值:无
//*函数描述  :ack:  0   应答
//           ack:  1   不应答
//************************************************/
//void HP6_IIC_SendAck(u8 ack)
//{
//	HP6_IIC_SCL_L;     //为了可以动数据线
//	//延时5us
//	Tim11_Delay_Us(10);
//	if(ack==0)
//	{
//		HP6_IIC_SDA_OUT_L;    //为了可以产生应答信号
//	}
//	else
//	{
//		HP6_IIC_SDA_OUT_H;    //为了可以产生不应答信号
//	}
//	
//	//延时5us
//	Tim11_Delay_Us(10);
//	
//	HP6_IIC_SCL_H;          //为了产生应答信号/不应答信号
//	
//	//延时5us
//	Tim11_Delay_Us(10);
//  	
//	//安全作用
//	HP6_IIC_SCL_L;
//}

///***********************************************
//*函数名    :HP6_IIC_ReceiveAck
//*函数功能  :IIC返回应答/不应答函数
//*函数参数  :无
//*函数返回值:u8
//*函数描述  :返回0   应答
//           返回1   不应答
//************************************************/
//u8 HP6_IIC_ReceiveAck(void) 
//{
//	u8 ack;
//	/*把数据线切换为输入模式*/
//	HP6_IIC_SCL_L;        //为了可以动数据线
//	HP6_IIC_SDA_OUT_H;    //把输出路堵死
//	
//	/*检测应答*/
//	HP6_IIC_SCL_L;        //

//	//延时5us
//	Tim11_Delay_Us(10);
//	
//	HP6_IIC_SCL_H;        //
//	
//	//延时5us
//	Tim11_Delay_Us(10);
//	
//	if(HP6_IIC_SDA_IN)    //
//	{
//		ack = 1;
//	}
//	else
//	{
//		ack = 0;
//	}
//	
//	//安全作用
//	HP6_IIC_SCL_L;
//	
//	return ack;
//}

///***********************************************
//*函数名    :HP6_IIC_SendByte
//*函数功能  :IIC发送一个字节函数
//*函数参数  :u8
//*函数返回值:无
//*函数描述  :
//************************************************/
//void HP6_IIC_SendByte(u8 data)
//{
//	for(u8 i = 0;i < 8;i++)
//	{
//		u8 i;
//	for(i=0;i<8;i++)     //循8次
//	{
//		HP6_IIC_SCL_L;         //为了写入数据
//		//延时5us
//	  Tim11_Delay_Us(10);
//		if(data & 0x80)    //判断最高位是什么
//		{
//		  HP6_IIC_SDA_OUT_H;
//		}
//		else
//		{
//			HP6_IIC_SDA_OUT_L;
//		}
//		//延时5us
//	  Tim11_Delay_Us(10);
//		HP6_IIC_SCL_H;          //帮助从机读取数据线的数据
//		//延时5us
//	  Tim11_Delay_Us(10);
//		data = data<<1;     //把此高位变成最高位
//	}
//	
//	//安全作用
//	HP6_IIC_SCL_L;
//	}
//}


///***********************************************
//*函数名    :HP6_IIC_ReceiveByte
//*函数功能  :IIC接收一个字节函数
//*函数参数  :无
//*函数返回值:u8 
//*函数描述  :
//************************************************/
//u8 HP6_IIC_ReceiveByte(void)
//{
//	u8 i;
//	u8 data;
//	/*将输出线切换为输入模式*/
//	HP6_IIC_SCL_L;
//  HP6_IIC_SDA_OUT_H;
//	
//	
//	/*接收一个字节数据*/
//	//帮助从机发送数据
//	
//	for(i=0;i<8;i++)
//	{
//		HP6_IIC_SCL_L;
//		//延时5us
//	  Tim11_Delay_Us(10);
//	 	HP6_IIC_SCL_H;
//		//延时5us
//  	Tim11_Delay_Us(10);
//		data = data << 1;
//		if(HP6_IIC_SDA_IN)
//		{
//			data = data | 0x01;
//		}
//  }
//	//安全
//	HP6_IIC_SCL_L;
//	
//	return data;
//}

//void HP6_IIC_IO_Init(void)
//{
////	//打开时钟
////	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);                     //rcc.h里面找    //没有手敲代码
////	
////	//GPIO控制器配置
////	GPIO_InitTypeDef GPIO_InitStruct = {0};       //定义gpio配置结构体变量
////	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;    //选择开漏模式
////	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
//// 	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_1;        //1号io口
////	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
////	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz; 
////  GPIO_Init(GPIOA,&GPIO_InitStruct);  

////	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;  //或开漏也行
////	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2; 
////	GPIO_Init(GPIOA,&GPIO_InitStruct); 
////	
////	GPIO_SetBits(GPIOA,GPIO_Pin_1 | GPIO_Pin_2);
//	
//	/*CSL初始化配置*/
//	//端口时钟使能
//	RCC->AHB1ENR |= (1<<0);    //打开时钟线
//	
//	//端口模式配置
//	GPIOA->MODER &= ~(3<<2);
//	GPIOA->MODER |= (1<<2);
//	
//	//输出类型配置
//	GPIOA->OTYPER |= (1<<1);    //SCL推挽   --- 时钟线
//	
//	//输出速度配置
//	GPIOA->OSPEEDR &= ~(3<<2);
//	
//	//上下拉配置
//	GPIOA->PUPDR &= ~(3<<2);
//	
//	
//	/*SDA初始化配置*/
//	//端口模式配置
//	GPIOA->MODER &= ~(3<<4);
//	GPIOA->MODER |= (1<<4);
//	
//	//输出类型配置
//	GPIOA->OTYPER &= ~(1<<2);    //SDA开漏  --- 数据线
//	
//	//输出速度配置
//	GPIOA->OSPEEDR &= ~(3<<4);
//	
//	//上下拉配置
//	GPIOA->PUPDR &= ~(3<<4);
//	
//	
//	/*空闲状态*/
//	GPIOA->ODR |= (1<<1);    //SCL初始高电平
//	GPIOA->ODR |= (1<<2);    //SDA初始高电平
//}

