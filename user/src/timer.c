#include "timer.h"

/********************************************************************
* 函数名 	：Tim5_Delay_Ms
* 函数功能	：定时器11的毫秒级延时函数
* 函数参数	：u32 ms  延时时间
* 函数返回值：void
* 函数说明	：
*			  APB1  84M  8400  10次/ms
*********************************************************************/
void Tim11_Delay_Ms(u32 ms)
{
	//打开定时器11的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM11, ENABLE);
	//选择单脉冲模式
	TIM_SelectOnePulseMode(TIM11,TIM_OPMode_Single);
	//定时器配置
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct = {0};
	TIM_TimeBaseInitStruct.TIM_ClockDivision =  TIM_CKD_DIV1;    //时钟分频--选择无分频
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;   //计数模式---选择向上计数
	TIM_TimeBaseInitStruct.TIM_Period = ms*10 -1;				//重载值
	TIM_TimeBaseInitStruct.TIM_Prescaler = 10000 - 1;		//预分频
	TIM_TimeBaseInit(TIM11, &TIM_TimeBaseInitStruct);
	//计数器清零
	TIM_ClearFlag(TIM11, TIM_FLAG_Update);
	//开始计数
	TIM_Cmd(TIM11,ENABLE);
	//等待计数完成
	while(!TIM_GetFlagStatus(TIM11,TIM_FLAG_Update));
	//关闭计数器
	TIM_Cmd(TIM11,DISABLE);
}

/********************************************************************
* 函数名 	：Tim11_Delay_Us
* 函数功能	：定时器5的微秒级延时函数
* 函数参数	：u32 us  延时时间
* 函数返回值：void
* 函数说明	：
*			  APB1  84M  42  2次/us
*********************************************************************/
void Tim11_Delay_Us(u32 us)
{
	//打开定时器11的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM11, ENABLE);
	//定时器配置
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct = {0};
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;    //时钟分频--选择无分频
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CKD_DIV1;    //时钟分频--选择无分频
	TIM_TimeBaseInitStruct.TIM_Period = us*2 -1;				//重载值
	TIM_TimeBaseInitStruct.TIM_Prescaler = 50 - 1;		//预分频

	TIM_TimeBaseInit(TIM11, &TIM_TimeBaseInitStruct);
	//选择单脉冲模式
	TIM_SelectOnePulseMode(TIM11,TIM_OPMode_Single);
	//计数器清零
	TIM_ClearFlag(TIM11, TIM_FLAG_Update);
	//开始计数
	TIM_Cmd(TIM11,ENABLE);
	//等待计数完成
	while(!TIM_GetFlagStatus(TIM11,TIM_FLAG_Update));
	//关闭计数器
	TIM_Cmd(TIM11,DISABLE);
}

void Timer9_Interrupt_Ms(u16 ms)
{
		//打开定时器9的时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9, ENABLE);

	//定时器配置
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct = {0};
	TIM_TimeBaseInitStruct.TIM_ClockDivision =  TIM_CKD_DIV1;    //时钟分频--选择无分频
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;   //计数模式---选择向上计数
	TIM_TimeBaseInitStruct.TIM_Period = ms*10 -1;				//重载值
	TIM_TimeBaseInitStruct.TIM_Prescaler = 10000 - 1;		//预分频
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM9, &TIM_TimeBaseInitStruct);
	
	//计数器清零
	TIM_ClearFlag(TIM9, TIM_FLAG_Update);
	
	//配置输出中断控制
	TIM_ITConfig(TIM9, TIM_IT_Update, ENABLE);
	
	//配置中断
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
	NVIC_InitTypeDef NVIC_InitStruct = {0};
	NVIC_InitStruct.NVIC_IRQChannel = TIM1_BRK_TIM9_IRQn ;			//确定中断
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;  			//使能中断响应通道
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&NVIC_InitStruct);
	
	//开始计数
	TIM_Cmd(TIM9,ENABLE);

}


/********************************************
PWM频率：Freq=CK_PSC/(PSC+1)/(ARR+1)=1000
PWM占空比：Duty=CCR/(ARR+1)=50%
PWM分辨率：Reso=1/(ARR+1)=1%

********************************************/

void TIM2_CH3_PWM_Init(void)
{
	//打开PB10引脚时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	
	//打开定时器2的时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	//使能GPIO口复用功能
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_TIM2);
	
	//GPIO控制器配置
	GPIO_InitTypeDef GPIO_InitStruct = {0};       //定义gpio配置结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;    //选择复用模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;   //选择输出推挽
 	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;        //10号io口
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL; //无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;  //选择低速
  GPIO_Init(GPIOB,&GPIO_InitStruct);            
	
	//定时器配置
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct = {0};
	TIM_TimeBaseInitStruct.TIM_ClockDivision =  TIM_CKD_DIV1;    //时钟分频--选择无分频
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;   //计数模式---选择向上计数
	TIM_TimeBaseInitStruct.TIM_Period = 100 -1;				//重载值ARR
	TIM_TimeBaseInitStruct.TIM_Prescaler = 1000 - 1;		//预分频PSC
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseInitStruct);
	
	//配置输出比较通道
	TIM_OCInitTypeDef TIM_OCInitStruct = {0};
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;   //选择输出比较模式
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;  //设置输出比较极性
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;  //输出使能
	TIM_OCInitStruct.TIM_Pulse = 0;  //设置CCR
	TIM_OC3Init(TIM2, &TIM_OCInitStruct);
	
	//计数器清零
	TIM_ClearFlag(TIM2, TIM_FLAG_Update);
	
	//开始计数
	TIM_Cmd(TIM2,ENABLE);
	
}

void PWM_SetComparel(u32 Compare)
{
	TIM_SetCompare3(TIM2,Compare);
}
