#include "sht20.h"
#include "iic.h"
#include "timer.h"

/***********************************************
*函数名    :SHT20_Init
*函数功能  :sht20初始化
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void SHT20_Init(void)
{
	/*IIC初始化*/
  IIC_IO_Init();  //iic所用io口初始化
	/*软复位*/
	SHT20_Soft();   	
	
}

/***********************************************
*函数名    :SHT20_Soft
*函数功能  :sht20软复位函数
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void SHT20_Soft(void)
{
	u8 ack;
	IIC_Start();
	IIC_SendByte(SHT20_ADDR_W);
	ack = IIC_ReceiveAck();
	if(ack == 1)
	{
		IIC_Stop();
		return;
	}
	
	IIC_SendByte(SHT20_SOFT);
	ack = IIC_ReceiveAck(); 
	if(ack == 1)
	{
		IIC_Stop();
		return;
	}
	
	IIC_Stop();
	Tim11_Delay_Ms(15);
}


/***********************************************
*函数名    :sht20_rec_val
*函数功能  :接收数据函数
*函数参数  :u8 cmd
*函数返回值:TRH_n
*函数描述  :
************************************************/
TRH_n SHT20_REC_VAL(u8 cmd)
{
	u8 ack;
	u8 temp_h,temp_l;
	u16 temp;
	TRH_n val;
	/*接收数据部分*/
	IIC_Start();                       //起始信号
	IIC_SendByte(SHT20_ADDR_W);      //发送器件地址---写
	ack = IIC_ReceiveAck();              //检测应答/不应答
	
	IIC_SendByte(cmd);               //发送命令
	ack = IIC_ReceiveAck();              //检测应答/不应答 
	
	/*等待测量完成*/
	//方法一延时
	//time11_delay_ms(85);
	
	//方法二等待应答
	while(1)
	{
		IIC_Start();                      //起始信号
		IIC_SendByte(SHT20_ADDR_R);    //发送器件地址---读
		ack = IIC_ReceiveAck();           //检测应答/不应答
	  if(ack==0)
		{
			break;
		}
	}
	
	/*读数据*/
	temp_h = IIC_ReceiveByte();          //接收高8位
	IIC_SendAck(0);                  //发送应答-----继续接收数据
	temp_l = IIC_ReceiveByte();         	//接收低8位
	IIC_SendAck(1);                	//发送不应答-----不再接收数据
	IIC_Stop();                       //停止信号函数
	
	/*把接收到的数据转换为温度或者湿度部分*/
	//合成数据
	temp = (temp_h<<8) | temp_l;      //把高8位和低8位合成一个数据
	
	//如果测温度
	if(cmd == SHT20_T)
	{
		val.t = -46.85 + 175.72 * temp / 65536;     //温度公式
	}
	//如果测湿度
	else if(cmd == SHT20_RH)
	{
		val.rh = -6 + 125 * temp / 65536;            //湿度公式
	}
	
	//返回共用体
	return val;
	
}

