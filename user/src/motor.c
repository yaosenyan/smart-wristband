#include "motor.h"

void Motor_Init(void)
{
		//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);                     //rcc.h里面找    //没有手敲代码
	
	//GPIO控制器配置
	GPIO_InitTypeDef GPIO_InitStruct = {0};       //定义gpio配置结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;    //选择通用输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;   //选择输出推挽
 	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;        //10号io口
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL; //无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;  //选择低速
  GPIO_Init(GPIOB,&GPIO_InitStruct);            
	
	//关电机
	MOTOR_OFF;
}

