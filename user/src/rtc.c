#include "rtc.h"  
/***********************************************
*函数名    :RTC_init
*函数功能  :rtc初始化函数
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void RTC_init(RTC_t time)
{
	//开启时钟
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_BKPSRAM, ENABLE);
	//备份奇存器访问使能
	PWR_BackupAccessCmd(ENABLE);
	
//	//解除RTC寄存器使能
//	RTC->WPR = 0xca;
//	RTC->WPR = 0x53;


if(RTC_ReadBackupRegister(RTC_BKP_DR10) != 700)
	{
	
	/*选择时钟源*/       //内部时钟，外部时钟
	//使能外部振荡器
		//RCC_LSICmd(ENABLE);
	RCC->CSR |= (1<<0);    //现在外部时钟
		
	
	//等待内部振荡器就绪   //等待脉冲过来		外部时钟晶振不起振
		while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) != SET){};
	//while(!(RCC->CSR & (1<<1))){};
		
		//选择RTCCLK时钟源
		RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);  //LSI频率为32000/LSE频率为32768Hz
		//使能时钟
		RCC_RTCCLKCmd(ENABLE);

	
	//配置预分频器
		RTC_InitTypeDef RTC_InitStruct = {0};
		RTC_InitStruct.RTC_AsynchPrediv = 0x80-1 ; //异步分频系数
		RTC_InitStruct.RTC_SynchPrediv = 0xFA-1; //同步分频系数(对异步分频后的时钟进一步分频) 128*256=32768
		RTC_InitStruct.RTC_HourFormat = RTC_HourFormat_24;
		RTC_Init(&RTC_InitStruct);


		//设置一个初始时间
		set_time(time);
		
		RTC_WriteBackupRegister(RTC_BKP_DR10, 00);	
	}

}



/***********************************************
*函数名    :set_time
*函数功能  :设置时间参数
*函数参数  :RTC_t time
*函数返回值:无
*函数描述  :
************************************************/
ErrorStatus set_time(RTC_t time)
{
	    
	RTC_TimeTypeDef RTC_TimeTypeInitStructure;
	
	RTC_TimeTypeInitStructure.RTC_Hours=time.h;
	RTC_TimeTypeInitStructure.RTC_Minutes=time.m;
	RTC_TimeTypeInitStructure.RTC_Seconds=time.s;
	
	RTC_DateTypeDef RTC_DateTypeInitStructure;
	
	RTC_DateTypeInitStructure.RTC_Date=time.day;
	RTC_DateTypeInitStructure.RTC_Month=time.mon;
	RTC_DateTypeInitStructure.RTC_WeekDay=time.week;
	RTC_DateTypeInitStructure.RTC_Year=time.year-2000;
	

	return (RTC_SetTime(RTC_Format_BIN,&RTC_TimeTypeInitStructure)&&RTC_SetDate(RTC_Format_BIN,&RTC_DateTypeInitStructure));
	//参数设置相关时间，把建立的时间SetTime返回，供主函数GetTime获取

	
}

/***********************************************
*函数名    :get_time
*函数功能  :获取当前时间函数
*函数参数  :无
*函数返回值:RTC_t
*函数描述  :
************************************************/
RTC_t get_time(void)
{
	RTC_t t;
	
	RTC_TimeTypeDef RTC_TimeStructure;
	RTC_DateTypeDef RTC_DateStructure;
	
	RTC_GetTime(RTC_Format_BIN,&RTC_TimeStructure);//获取时间
	RTC_GetDate(RTC_Format_BIN,&RTC_DateStructure);//获取日期
	
	t.h = RTC_TimeStructure.RTC_Hours;
	t.m = RTC_TimeStructure.RTC_Minutes;
	t.s = RTC_TimeStructure.RTC_Seconds;
	
	t.year = RTC_DateStructure.RTC_Year+2000 ;
	t.week = RTC_DateStructure.RTC_WeekDay;
	t.mon = RTC_DateStructure.RTC_Month;
	t.day = RTC_DateStructure.RTC_Date;

	
	//返回数据
	return t;          //返回结构体变量
}






