#include "usart.h"
#include "stdio.h"


void Usart_Init(u32 brr)
{
	//打开时钟	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);     //打开GPIO控制时钟	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);   //打开USART1控制时钟
	
	//使能GPIO口复用功能
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
	
	//配置GPIO口
		//把PA9配置为复用功能、输出推挽、50MHz输出速度、无上下拉
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;		//配置为复用模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9;				//PA9（选择9号GPIO口）
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;	//配置50MHz
	GPIO_Init(GPIOA, &GPIO_InitStruct); 
		//把PA10配置为复用功能、无上下拉
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;				//PA10（选择10号GPIO口）
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	//配置串口通信（串口控制器进行配置）
	USART_InitTypeDef USART_InitStruct = {0};
	USART_InitStruct.USART_BaudRate =  brr;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;	//选择无硬件流控制
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;									//使能接收和发送
	USART_InitStruct.USART_Parity = USART_Parity_No;										//选择无奇偶校验
	USART_InitStruct.USART_StopBits = USART_StopBits_1;									//选择1位停止位
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;						//选择8位数据位
	USART_Init(USART1, &USART_InitStruct);
	
	//中断配置
	NVIC_InitTypeDef NVIC_InitStruct = {0};
	NVIC_InitStruct.NVIC_IRQChannel = USART1_IRQn;			//确定中断
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;  			//使能中断响应通道
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
	NVIC_Init(&NVIC_InitStruct);
	
	//中断使能
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);		//使能接收中断
	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);  //使能空闲中断
	//打开串口控制器 
	USART_Cmd(USART1, ENABLE);
}

void Usart1_Send_Data(u8 data)
{
	//等待上一个数据发送完成
	while(!USART_GetFlagStatus(USART1, USART_FLAG_TXE)){};
	//把data发送出去
	USART_SendData(USART1, data);
}

u8 Usart1_Recv_Data(void)
{
	//等待数据接收完成
	while(!USART_GetFlagStatus(USART1, USART_FLAG_RXNE));
	
	//返回数据
	return USART_ReceiveData(USART1);
}




//重定向 --- 把串口发送数据函数定向为printf

#if 1
#pragma import(__use_no_semihosting)             
	//标准库需要的支持函数                 
	struct __FILE 
	{ 
		int handle; 
		/* Whatever you require here. If the only file you are using is */ 
		/* standard output using printf() for debugging, no file handling */ 
		/* is required. */ 
	}; 
	/* FILE is typedef’ d in stdio.h. */ 
	FILE __stdout;       
	//定义_sys_exit()以避免使用半主机模式    
	int _sys_exit(int x) 
	{ 
		x = x; 
	} 
	//重定义fputc函数 
	int fputc(int ch, FILE *f)
	{      
		while((USART1->SR&0X40)==0);//循环发送,直到发送完毕   
		USART1->DR = (u8) ch;      
		return ch;
	}
#endif 

	
	