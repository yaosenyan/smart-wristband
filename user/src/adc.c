#include "adc.h"
/***********************************************
*函数名    :adc1_ch3_Init
*函数功能  :ADC1通道3初始化函数
*函数参数  :无
*函数返回值:无
*函数描述  :PA3-------ADC1_CH3------四方向按键  原理图
************************************************/
void Adc1_Ch3_Init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);                     //rcc.h里面找    //没有手敲代码
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);
	
	//GPIO控制器配置  模拟模式
	GPIO_InitTypeDef GPIO_InitStruct = {0};       //定义gpio配置结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;    //选择模拟模式
 	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3;        //3号io口
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL; //无上下拉
  GPIO_Init(GPIOA,&GPIO_InitStruct);  

	//选择规则组输入通道
	ADC_RegularChannelConfig(ADC1,ADC_Channel_3,1,ADC_SampleTime_480Cycles);

	
	//ADC控制器配置
	ADC_InitTypeDef ADC_InitStruct = {0};
	ADC_InitStruct.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;  //不用外部触发，选择软件触发
	ADC_InitStruct.ADC_ContinuousConvMode = DISABLE;  //单次转换模式
	ADC_InitStruct.ADC_DataAlign = ADC_DataAlign_Right;  //数据右对齐模式
	ADC_InitStruct.ADC_NbrOfConversion = 1;   //一次转换一个通道
	ADC_InitStruct.ADC_Resolution = ADC_Resolution_12b;  //选择12位分辨率
	ADC_InitStruct.ADC_ScanConvMode = ENABLE;  //使能扫描模式

	ADC_Init(ADC1, &ADC_InitStruct);

	//ADC通用配置
	ADC_CommonInitTypeDef ADC_CommonInitStruct = {0};
	ADC_CommonInitStruct.ADC_Mode = ADC_Mode_Independent;    //独立模式
	ADC_CommonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;  //DMA失能
	ADC_CommonInitStruct.ADC_Prescaler = ADC_Prescaler_Div4;  //预分频4分频   //ADCCLK=PCLK2/4=84/4=21Mhz,ADC 时钟最好不要超过 36Mhz
	
	ADC_CommonInit(&ADC_CommonInitStruct);

	
	//SQR1\2\3
//	ADC1->SQR1 &= ~(15<<20);      //1次转换
//	ADC1->SQR3 &= ~(31<<0);       //清零
//	ADC1->SQR3 |= (3<<0);         //顺序规则中的第一次转换
	
	
	//ADC控制器使能
	ADC_Cmd(ADC1,ENABLE);

	
}

/***********************************************
*函数名    :get_adc1_data
*函数功能  :获取adc1的转换数据
*函数参数  :无
*函数返回值:u16 
*函数描述  :PA3-----ADC1_CH3------四方向按键
************************************************/
u16 Get_Adc1_Data(void)
{
	u16 data;
	//开始转换
	ADC1->CR2 |= (1<<30);        //命令，开始命令
	
	//等待转换完成
	while(!(ADC1->SR & (1<<1))); //等待转换完成
	
	//获取转换数据到变量
	data = ADC1->DR;             //读取数据
	//返回转换值
	return data;
}

























