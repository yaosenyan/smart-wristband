#include "function.h"
#include "oled.h"
#include "font.h"
#include "timer.h"
#include "key.h"
#include "string.h"
#include "stdio.h"
#include "rtc.h"
#include "tkr.h"
#include "nvic.h"
#include "sht20.h"
#include "hp6.h"
#include "ikun.h"

/***********************************************
*函数名    :start_k
*函数功能  :开机界面
*函数参数  :无
*函数返回值:无
*函数描述  :
************************************************/
void start_k(void)
{
	/*开机界面显示*/
	oled_dis_pic(0,0,start_pic,128,64);
	TIM2->CCR3 = 900;            //开电机
	Tim11_Delay_Ms(500);        //延时
	TIM2->CCR3 = 0;              //关电机      
	OLED_clear();                //清屏	
}


/***********************************************
*函数名    :key_page
*函数功能  :按键控制页编号及功能函数
*函数参数  :无
*函数返回值:无
*函数描述  :开机震动和开机页面显示
************************************************/
u8 key;
u8 page = 1;
void key_page(void)
{
	while(1)
	{
		key = Fun_Key_Scan();   //按键识别函数
		if(key == KEY_RIGHT)
		{
			if(dis_flag == 1)
			{
				page += 1;
				if(page == 7)
				{
					page = 1;
				}
			}
			dis_flag = 1;
			a_flag = 1;     //解锁标志位
			tim_flag[3] = 0;
			OLED_writeByte(0xAF,OLED_CMD);
		}
		if(key == KEY_LEFT)
		{
			if(dis_flag == 1)
			{
				page -= 1;
				if(page == 0)
				{
					page = 6;
				}
			}
			dis_flag = 1;
			a_flag = 1;
			tim_flag[3] = 0;
			OLED_writeByte(0xAF,OLED_CMD);
		}
		switch(page)
		{
			case 1:function_page1();break;    //主页面
			case 2:function_page2();break;    //查看时间界面
			case 3:function_page3();break;    //查看时间界面
			case 4:function_page4();break;    //测量心率
			case 5:function_page5();break;    //测量血压
			case 6:function_page6();break;    //测步数
		}
		
		
		
		
	}
	
	
}

/***********************************************
*函数名    :function_page1
*函数功能  :手环的首页面函数
*函数参数  :无
*函数返回值:无
*函数描述  :1.显示动态图
						2.显示名字
************************************************/
u8 a_flag = 1;
char a[4][10] = {"姚森炎","张晓静","张智铭","祝兆邦"};
void function_page1(void)
{
	if(a_flag == 1)
	{
		HP_6_Closebp();//关闭血压测量
		OLED_clear();
		a_flag = 0;
		oled_dis_hz_str(0,70,(u8 *)&a[0][0],16,16);
		oled_dis_hz_str(2,70,(u8 *)&a[1][0],16,16);
		oled_dis_hz_str(4,70,(u8 *)&a[2][0],16,16);
		oled_dis_hz_str(6,70,(u8 *)&a[3][0],16,16);
	}
	//oled_dis_pic(0,2,(u8 *)(TKR64x64+pic_n),64,64); //太空人
		oled_dis_pic(0,2,(u8 *)(IKUN+pic_n),35,64);
}


/***********************************************
*函数名    :function_page2
*函数功能  :2号页功能
*函数参数  :无
*函数返回值:无
*函数描述  :显示时间
						利用定时中断一秒去获取当前时间
************************************************/
void function_page2(void)
{
	u8 buff[20]={0};                 
	/*本页刷一次*/
	if(a_flag == 1)        //标志位   ---    只执行一次
	{
		//pic_n = 0;
		OLED_clear();
		a_flag = 0;          //锁死标志位
	}
	/*本页刷多次*/
	//显示日期
	sprintf((char *)buff,"%d年%d月%d日",val.year,val.mon,val.day);
	oled_dis(0,15,buff,16,16); //显示字符串去显示出来
	//显示时间
	sprintf((char *)buff,"%02d:%02d:%02d",val.h,val.m,val.s);
	oled_dis(3,30,buff,16,16);  //显示字符串去显示出来
	//显示星期
  sprintf((char *)buff,"星期:%d",val.week);
	oled_dis(6,40,buff,16,16); //显示字符串去显示出来   	
}

/***********************************************
*函数名    :function_page3
*函数功能  :2号页功能
*函数参数  :无
*函数返回值:无
*函数描述  :显示时间
						利用定时中断一秒去获取当前时间
************************************************/
void function_page3(void)
{                
		TRH_n sum;
	u8 buff[10]={0};
	/*本页刷一次*/
	if(a_flag == 1)
	{
		HP_6_CloseRate();
		OLED_clear();
	  if(val.h>=6 && val.h<18)   //显示图片
		{
			oled_dis_pic(0,0,sun,64,64);  //白天图片
		}
		else
		{
			oled_dis_pic(0,0,night,64,64);  //晚上图片
		}
		//pic_n = 0;
		a_flag = 0;
	}

	/*本页刷多次*/   
	//显示温度
	sum = SHT20_REC_VAL(SHT20_T);   //测量温度
	sprintf((char *)buff,"T:%0.1f℃",sum.t);    //数据合成
  oled_dis(4,80,buff,16,16);      //显示合成后的buff
	
	
	//显示湿度
  sum = SHT20_REC_VAL(SHT20_RH); //检测湿度
	sprintf((char *)buff,"RH:%d%%",sum.rh);    //数据合成
  oled_dis(1,80,buff,16,16);      //显示合成后的buff
}


/***********************************************
*函数名    :function_page4
*函数功能  :4号页功能
*函数参数  :无
*函数返回值:无
*函数描述  :显示心率
************************************************/

void function_page4(void)
{
	u8 buff[10]={0};
	u8 heart_val;
	static u8 old_val = 0;
	/*本页刷一次*/
	if(a_flag == 1)
	{
		OLED_clear();
		HP_6_Closebp();//关闭血压测量
	  //oled_dis_pic(0,2,heart,54,54);
		//pic_n = 0;
		oled_dis(1,80,(u8 *)"心率",16,16);
		oled_dis(4,80,(u8 *)"test",16,16);
		oled_dis_pic(0,2,(u8 *)xl_pic,64,64);
		HP_6_OpenRate();//打开心率测量
		a_flag = 0;
	}
	/*本页刷多次*/
	//心率动态图显示
	//oled_dis_pic(0,2,(u8 *)(dis_heart64x64+heart_n),64,64);
	
	
  //获取数据
	
	HP_6_GetRateResult(&heart_val);
	if(heart_val != 0 && old_val != heart_val)  
	{
		OLED_clear();
		oled_dis_pic(0,2,(u8 *)xl_pic,64,64);
		old_val = heart_val;
		sprintf((char *)buff,"%3d",heart_val);
		oled_dis(4,65,buff,16,16);
	}
}

/***********************************************
*函数名    :function_page5
*函数功能  :5号页功能
*函数参数  :无
*函数返回值:无
*函数描述  :显示血压   一分钟
************************************************/
void function_page5(void)
{
	u8 buff[10];
	u8 bp_buff[24]={0};
	/*本页刷一次*/
	if(a_flag == 1)
	{
		OLED_clear();
		HP_6_CloseRate();
		//oled_dis_pic(0,2,bmp,54,54);
	  oled_dis(1,80,(u8 *)"血压",16,16);
		oled_dis(4,80,(u8 *)"test",16,16);
		HP_6_Openbp();
		oled_dis_pic(0,2,(u8 *)xy_pic,64,64);
		//pic_n = 0;
		a_flag = 0;
	}
	
	/*本页刷多次*/
	//oled_dis_pic(0,2,(u8 *)(dis_bmp2+bmp_n),54,54);
  HP_6_Get_bpResult(bp_buff);
	if(bp_buff[7]==2)
	{
		HP_6_Closebp();
		HP_6_Openbp();
	}
	if(bp_buff[7]==1)
	{
		OLED_clear();
		oled_dis_pic(0,2,(u8 *)xy_pic,64,64);
		sprintf((char *)buff,"H:%3dPa",bp_buff[10]);
		oled_dis(3,70,buff,16,16);
		sprintf((char *)buff,"L:%3dPa",bp_buff[11]);
		oled_dis(5,70,buff,16,16);      
		HP_6_Closebp();
		HP_6_Openbp();
	}
	
	
	
}


void function_page6(void)
{
	u8 buff[20]={0};                 
	/*本页刷一次*/
	if(a_flag == 1)        //标志位   ---    只执行一次
	{
		//pic_n = 0;
		OLED_clear();
		a_flag = 0;          //锁死标志位
	}

	/*本页刷多次*/
	//显示步数
	sprintf((char *)buff,"步数:%d",mpu_val);
	oled_dis(2,70,buff,16,16); //显示字符串去显示出来
  	
}
