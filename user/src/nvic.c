#include "nvic.h"
#include "motor.h"
#include "string.h"
#include "led.h"
#include "stdio.h"
#include "tkr.h"
#include "oled.h"
#include "rtc.h"
#include "function.h"
#include "mpu6050.h"

u8 usart1_buff[30];
void USART1_IRQHandler(void)
{
	static u8 i = 0;
	//接收中断信号触发
	if(USART_GetITStatus(USART1, USART_IT_RXNE))         //判断如果是接收中断
	{
		USART_ClearITPendingBit(USART1, USART_IT_RXNE);  //清除中断标志
		
		usart1_buff[i++] = USART_ReceiveData(USART1);  //接收数据
		
	}
	//空闲中断信号触发
	if(USART_GetITStatus(USART1, USART_IT_IDLE))  
	{
		//清除空闲中断标志位
		USART1->SR;  			//读状态寄存器
		USART1->DR;  			//读数据寄存器
		//数据处理
		usart1_buff[i] = '\0';
		i = 0;
		
		//判断数据执行操作
		if(strncmp((char *)usart1_buff,"开灯",2) == 0)
		{
			LED1_ON;
		}
		else if(strncmp((char *)usart1_buff,"关灯",2) == 0)
		{

			LED1_OFF;

		}
		
	}
}

/******************************************
*函数名    ：TIM1_BRK_TIM9_IRQHandler                 
*函数功能  ：定时中断服务函数
*函数参数  ：无
*函数返回值：无
*函数描述  ：无
*********************************************/
u32 tim_flag[10] = {0};
u8 pic_n=0;
RTC_t val;
u8 mpu_val;
u8 dis_flag = 1;
void TIM1_BRK_TIM9_IRQHandler(void)
{

	if(TIM_GetITStatus(TIM9,TIM_IT_Update) == SET)
	{
		//把中断标志位置0
		TIM_ClearITPendingBit(TIM9,TIM_IT_Update);
		
		//紧急事件
		tim_flag[1]++;
		tim_flag[2]++;
		tim_flag[3]++;
		tim_flag[4]++;
		
		//RTC
		if(tim_flag[1]==1000)
		{
			tim_flag[1] = 0;
			val = get_time();   //获取时间
		}
		//动态图	
		//const u8 *pics[] = {pic_0,pic_1,pic_2,pic_3,pic_4,pic_5,pic_6};	
		
		if(tim_flag[2]==12)   //50太空人
		{
			tim_flag[2]=0;
			
				//oled_dis_pic(0,30,pics[pic_n],64,64);
				
				pic_n++;
//				if(pic_n>101)
//				{
//					pic_n=0;
//				}
					if(pic_n>59)  //59IKUN  
					{
						pic_n=0;
					}
		 
		}
		//10秒熄屏
		if(tim_flag[3]==10000 && (page != 4 && page != 5))
		{
			tim_flag[3] = 0;
			OLED_writeByte(0xAE,OLED_CMD);   //获取时间
			dis_flag = 0;
		}
		
		if(tim_flag[4]==50)
		{
			tim_flag[4] = 0;
			mpu_val = StepCounter(MPU6050_GetData().AccX, MPU6050_GetData().AccY, MPU6050_GetData().AccZ);
		}
		
	}
	
}
