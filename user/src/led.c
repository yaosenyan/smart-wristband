#include "led.h"

void Led1_Init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);                     //rcc.h里面找    //没有手敲代码
	
	//GPIO控制器配置
	GPIO_InitTypeDef GPIO_InitStruct = {0};       //定义gpio配置结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;    //选择通用输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;   //选择输出推挽
 	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7;        //7号io口
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL; //无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;  //选择低速
  GPIO_Init(GPIOA,&GPIO_InitStruct);            
	
	//关灯
	LED1_OFF;
}