#include "oled.h"
#include "timer.h"
#include "font.h"
#include "string.h"
///***********************************************
//*函数名    :spi1_init
//*函数功能  :SPI1初始化
//*函数参数  :无
//*函数返回值:无
//*函数描述  :SPI1_SCK-------PB3-------复用输出
//            SPI1_MOSI------PB5-------复用输出
//						00模式
//************************************************/
//void spi1_init(void)
//{
//	//打开时钟
//	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
//	//用GPIO控制器配置GPIO口
//	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
//	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_3|GPIO_Pin_5;				//PB3、5（选择3、5号GPIO口）
//	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
//	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;	//配置50MHz
//	GPIO_Init(GPIOB, &GPIO_InitStruct); 

//	//空闲状态
//	OLED_SPI_SCL_L;			//时钟线低电平
//	OLED_MOSI_H;					//数据线高电平

//}

///********************************************************************
//*	函数名：OLED_Spi_Byte
//*	函数功能：SPI传输一个字节函数
//*	函数参数：u8 data
//*	函数返回值：void
//*	函数描述：
//*							时钟线为下降沿的时候发送数据
//*********************************************************************/

//void spi1_byte(u8 data)
//{
//	
//	
//	for(u8 i=0;i<8;i++)
//	{	
//		OLED_SPI_SCL_L;
//		if(data & 0x80)
//		{
//			OLED_MOSI_H;
//		}
//		else
//		{
//		  OLED_MOSI_L;
//		}
//		OLED_SPI_SCL_H;
//		data <<= 1;
//	}
//		
//}

/***********************************************
*函数名    :spi1_init
*函数功能  :SPI1初始化
*函数参数  :无
*函数返回值:无
*函数描述  :SPI1_SCK-------PB3-------复用输出
            SPI1_MOSI------PB5-------复用输出
						00模式
************************************************/
void spi1_init(void)
{
	/*IO口控制器配置*/
	//端口时钟使能
	RCC->AHB1ENR |= (1<<1);
	
	//端口模式配置
	GPIOB->MODER &= ~((3<<6) | (3<<10));
	GPIOB->MODER |= ((2<<6) | (2<<10));
	
	//输出类型配置
	GPIOB->OTYPER &= ~((1<<3) | (1<<5));
	
	//输出速度配置
	GPIOB->OSPEEDR &= ~((3<<6) | (3<<10));
	GPIOB->OSPEEDR |= ((2<<6) | (2<<10));
	
	//上下拉配置
	GPIOB->PUPDR &= ~((3<<6) | (3<<10));
	
	//复用寄存器配置
	GPIOB->AFR[0] &= ~((15<<12) | (15<<20));
	GPIOB->AFR[0] |= ((5<<12) | (5<<20));
	
	/*SPI控制器配置*/
  //SPI时钟使能
	RCC->APB2ENR |= (1<<12);
	
	//CR1
	SPI1->CR1 &= ~(1<<15);    //双线单向模式
	SPI1->CR1 &= ~(1<<11);    //8位数据帧格式
	SPI1->CR1 &= ~(1<<10);    //全双工
	SPI1->CR1 |= (1<<9);      //选择软件管理
	SPI1->CR1 |= (1<<8);      //允许通信条件
	SPI1->CR1 &= ~(1<<7);     //先发高位
	SPI1->CR1 &= ~(7<<3);     //2分频
	SPI1->CR1 |= (1<<2);      //主配置
	SPI1->CR1 &= ~(1<<1);     //时钟极性为0
	SPI1->CR1 &= ~(1<<0);     //时钟相位为0
	
	//SPI使能
	SPI1->CR1 |= (1<<6);

}



/***********************************************
*函数名    :spi1_byte
*函数功能  :SPI1传输一个字节数据函数
*函数参数  :u8 data
*函数返回值:u8
*函数描述  :
************************************************/
u8 spi1_byte(u8 data)
{
	u8 val;
	//等待数据发送完成
	while(!(SPI1->SR & (1<<1)));
	//将data赋值给DR
	SPI1->DR = data;
	
	//等待接收数据完成
	while(!(SPI1->SR & (1<<0)));
	//将DR赋值给一个变量
	val = SPI1->DR;
	//返回变量的值
	return val;
}


/********************************************************************
*	函数名：OLED_Io_Init
*	函数功能：OLED的管脚初始化函数
*	函数参数：void
*	函数返回值：void
*	函数描述：
*					OLED_CS： OLED 片选信号。--------------------------------PB7     //通用推挽输出
*					OLED_RES：硬复位 OLED。 ---------------------------------PB13   //通用推挽输出
*					OLED_D/C：命令/数据标志（0，读写命令； 1，写数据）-------PA15   //通用推挽输出
*********************************************************************/
void OLED_IO_init(void)
{
	//打开时钟
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	//用GPIO控制器配置GPIO口
	GPIO_InitTypeDef GPIO_InitStruct = {0}; 			//定义结构体变量
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;		//配置为输出模式
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;		//配置输出推挽
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_7|GPIO_Pin_13;			
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;	//配置无上下拉
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;	//配置2MHz
	GPIO_Init(GPIOB, &GPIO_InitStruct); 
	
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_15;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	//断开通讯
	OLED_CS_H;
	
}

/**************************************************************************************
*函数名    ：OLED_RST
*函数功能  ：OLED屏幕硬件复位函数
*函数参数  ：无
*函数返回值：无
*函数描述  ：			
****************************************************************************************/
void OLED_RST(void)
{
	OLED_RST_L;
	Tim11_Delay_Ms(100);
	OLED_RST_H;
}


/**********************************************************************
*函数名    ：OLED_writeByte
*函数功能  ：主控芯片发送数据/命令到OLED
*函数参数  ：u8 data,u8 cmd_data
*函数返回值：无
*函数描述  ：要发送命令,cmd_data 传 0    OLED_CMD
             要发送数据,cmd_data 传 1    OLED_DAT     
************************************************************************/
void OLED_writeByte(u8 data,u8 cmd_data)
{
	OLED_CS_L;
	
	(cmd_data)?(OLED_CD_H):(OLED_CD_L);
	
	spi1_byte(data);
	
	OLED_CS_H;
	
	
}




/*******************************************************
*函数名    ：OLED_init
*函数功能  ：OLED屏幕初始化配置
*函数参数  ：无
*函数返回值：无
*函数描述  ：   
*********************************************************/
void OLED_init(void)
{
	//SPI初始化配置
	spi1_init();
	
	//OLED_IO初始化配置
	OLED_IO_init();
	
	//复位程序
	OLED_RST();
	
	//初始化命令驱动
	//OLED初始化配置命令
	OLED_writeByte(0xAE,OLED_CMD);//--turn off oled panel
	OLED_writeByte(0x02,OLED_CMD);//---SET low column address
	OLED_writeByte(0x10,OLED_CMD);//---SET high column address
	OLED_writeByte(0x40,OLED_CMD);//--SET start line address  SET Mapping RAM Display Start Line (0x00~0x3F)
	OLED_writeByte(0x81,OLED_CMD);//--SET contrast control register
	OLED_writeByte(0xff,OLED_CMD); // SET SEG Output Current Brightness
	OLED_writeByte(0xA1,OLED_CMD);//--SET SEG/Column Mapping     0xa0左右反置 0xa1正常
	OLED_writeByte(0xC8,OLED_CMD);//SET COM/Row Scan Direction   0xc0上下反置 0xc8正常
	OLED_writeByte(0xA6,OLED_CMD);//--SET normal display
	OLED_writeByte(0xA8,OLED_CMD);//--SET multiplex ratio(1 to 64)
	OLED_writeByte(0x3f,OLED_CMD);//--1/64 duty
	OLED_writeByte(0xD3,OLED_CMD);//-SET display offSET	Shift Mapping RAM Counter (0x00~0x3F)
	OLED_writeByte(0x00,OLED_CMD);//-not offSET
	OLED_writeByte(0xd5,OLED_CMD);//--SET display clock divide ratio/oscillator frequency
	OLED_writeByte(0x80,OLED_CMD);//--SET divide ratio, SET Clock as 100 Frames/Sec
	OLED_writeByte(0xD9,OLED_CMD);//--SET pre-charge period
	OLED_writeByte(0xF1,OLED_CMD);//SET Pre-Charge as 15 Clocks & Discharge as 1 Clock
	OLED_writeByte(0xDA,OLED_CMD);//--SET com pins hardware configuration
	OLED_writeByte(0x12,OLED_CMD);
	OLED_writeByte(0xDB,OLED_CMD);//--SET vcomh
	OLED_writeByte(0x40,OLED_CMD);//SET VCOM Deselect Level
	OLED_writeByte(0x20,OLED_CMD);//-SET Page Addressing Mode (0x00/0x01/0x02)
	OLED_writeByte(0x02,OLED_CMD);//
	OLED_writeByte(0x8D,OLED_CMD);//--SET Charge Pump enable/disable
	OLED_writeByte(0x14,OLED_CMD);//--SET(0x10) disable
	OLED_writeByte(0xA4,OLED_CMD);// Disable Entire Display On (0xa4/0xa5)
	OLED_writeByte(0xA6,OLED_CMD);// Disable Inverse Display On (0xa6/a7) 
	OLED_writeByte(0xAF,OLED_CMD);//--turn on oled panel
	OLED_writeByte(0xAF,OLED_CMD); /*display ON*/

	
	//清屏函数
	OLED_clear();	
}

/**************************************************************************************
*函数名    ：OLED_clear
*函数功能  ：OLED清屏函数
*函数参数  ：无
*函数返回值：无
*函数描述  ： 
****************************************************************************************/
void OLED_clear(void)
{
	u8 page,list_cont;
	/*页循环*/
	for(page=0;page<8;page++)
	{
		//确定页地址
		OLED_writeByte(0xB0+page,OLED_CMD);
		//每页设定列的起始地址  2号列  0x02     0001 0000       0000 0010
		OLED_writeByte(0x10,OLED_CMD);
		OLED_writeByte(0x02,OLED_CMD);
		
		/*每页中根据列数传 数据个数 的循环*/     //根据列数算出的传入数据的次数循环
		for(list_cont=0;list_cont<129;list_cont++)
		{
			OLED_writeByte(0x00,OLED_DAT);
		}
	}
}

/*********************************************
*函数名    ：OLED_setstart
*函数功能  ：确定显示起始位置函数   哪页的哪列
*函数参数  ：u8 page  页号    u8 list   列号
*函数返回值：无
*函数描述  ：
			通过此函数能确定要显示的起始位置
*********************************************/
void OLED_setstart(u8 page,u8 list)
{
	  //确定页地址
		OLED_writeByte(0xB0+page,OLED_CMD);
		//每页设定列的起始地址  2号列  0x02
		OLED_writeByte((list>>4)|0x10,OLED_CMD); 
		OLED_writeByte(list&0x0f,OLED_CMD);
}

/************************************************
*函数名    ：oled_dis_char16
*函数功能  ：显示一个16*16的字符
*函数参数  ：u8 page,u8 list,u8 ch
*函数返回值：无
*函数描述  ：16*16  实际是 8*16   字高占用2页	
						 page:0~7
						 list:2~128
*************************************************/
void oled_dis_char16(u8 page,u8 list,u8 ch)
{
	u8 i,j;
	u8 n;
	/*计算要显示的字符与空格的偏移值*/
	n = ch - ' ';         //
	
	/*显示*/
	for(i=0;i<2;i++)                           //页循环
	{
		OLED_setstart(page+i,list);            //+i人为换页
		for(j=0;j<8;j++)                       //列循环
		{
			OLED_writeByte(F16X16[n*16+j+8*i],OLED_DAT);
		}
	}
	
}

/************************************************
*函数名    ：oled_dis_char24
*函数功能  ：显示一个32*24的字符
*函数参数  ：u8 page,u8 list,u8 ch
*函数返回值：无
*函数描述  ：32*24  实际是 16*24   字高占用3页	
						 page:0~7
						 list:2~128
*************************************************/
void oled_dis_char24(u8 page,u8 list,u8 ch)
{
	u8 i,j;
	u8 n;
	/*计算要显示的字符与空格的偏移值*/
	n = ch - ' ';
	
	/*显示*/
	for(i=0;i<3;i++)
	{
		OLED_setstart(page+i,list);
		for(j=0;j<16;j++)
		{
			OLED_writeByte(F32X24[n*48+j+16*i],OLED_DAT);
		}
	}
	
}

/************************************************
*函数名    ：oled_dis_char
*函数功能  ：显示一个字符
*函数参数  ：u8 page,u8 list,u8 ch
*函数返回值：无
*函数描述  ：w:形式宽度（实际宽度在程序中处理）   32
            h:形式高度（/8）
						 page:0~7
						 list:2~128
*************************************************/
void oled_dis_char(u8 page,u8 list,u8 ch,u8 w,u8 h)
{
	u8 i,j;
	u8 n;
	/*计算要显示的字符与空格的偏移值*/
	n = ch - ' ';
	
	/*显示*/
	for(i=0;i<h/8;i++)
	{
		OLED_setstart(page+i,list);
		for(j=0;j<w/2;j++)
		{
			if(w==16 && h==16)
			{
				OLED_writeByte(F16X16[n*16+j+8*i],OLED_DAT);
			}
			else if(w==32 && h==24)
			{
				OLED_writeByte(F32X24[n*48+j+16*i],OLED_DAT);
			}
			
		}
	}
	
}

/************************************************
*函数名    ：oled_dis_hz
*函数功能  ：显示一个的汉字
*函数参数  ：u8 page,u8 list,u8 *hz,u8 w,u8 h
*函数返回值：无
*函数描述  ：
						 page:0~7
						 list:2~128
*************************************************/
void oled_dis_hz(u8 page,u8 list,u8 *hz,u8 w,u8 h)
{
	u8 n=0;
	u8 i,j;
	/*计算要显示的数据与字库第一个字的偏移值*/
	while(table[2*n] != '\0')
	{
		if(*hz==table[2*n] && *(hz+1)==table[2*n+1])
		{
			break;
		}
		n++;
	}
	/*如果汉字库里面没有输入的汉字*/
	if(n == strlen((char *)table)/2)
	{
		return ;
	}
	
	/*显示*/
		w=h;
	//页循环
	for(i=0;i<h/8;i++)
	{
		//确认初始地址
		OLED_setstart(page+i,list);
	
		for(j=0;j<w;j++)
		{
			if(h==16 && w==16)
			{
				OLED_writeByte(hz16[n*32+j+16*i],OLED_DAT);
			}
			else if(h==24 && w==24)
			{
				OLED_writeByte(hz24[n*72+j+24*i],OLED_DAT);
			}
		}
		
	}
}

/************************************************
*函数名    ：oled_dis_str
*函数功能  ：显示一个字符串
*函数参数  ：u8 page,u8 list,u8 ch,u8 w,u8 h
*函数返回值：无
*函数描述  ：w:形式宽度（实际宽度在程序中处理）   32
            h:形式高度（/8）
						 page:0~7
						 list:2~128
*************************************************/

void oled_dis_str(u8 page,u8 list,u8 *str,u8 w,u8 h)
{
	while(*str!= '\0')
	{
		oled_dis_char(page,list,*str,w,h);
		str++;
		list+=w/2;
		if(list>128-w/2)
		{
			page+=h/8;
			list=2;
		}
	}
}

/************************************************
*函数名    ：oled_dis_hz_str
*函数功能  ：显示一个的汉字串
*函数参数  ：u8 page,u8 list,u8 *hz,u8 w,u8 h
*函数返回值：无
*函数描述  ：
						 page:0~7
						 list:2~128
*************************************************/
void oled_dis_hz_str(u8 page,u8 list,u8 *hz,u8 w,u8 h)
{
	while(*hz!='\0')
	{
		oled_dis_hz(page,list,hz,w,h);
		//显示下一个汉字
		hz+=2;
		//屏幕
		list += w;
		
		if(list>128-w)
		{
			page += h/8;
			list = 2;
		}
	}
}



/************************************************
*函数名    ：oled_dis
*函数功能  ：显示一串汉字和字符
*函数参数  ：u8 page,u8 list,u8 *hz,u8 w,u8 h
*函数返回值：无
*函数描述  ：
						 page:0~7
						 list:2~128
*************************************************/
void oled_dis(u8 page,u8 list,u8 *str,u8 w,u8 h)
{
	while(*str!='\0')                         //遍历每个数组中的每个元素
	{
		if(*str>=32 && *str<=127)               //判断是否为字符 
		{
			oled_dis_char(page,list,*str,w,h);    //调用显示一个字符函数
			str++;                                //换数组下一个元素
			list+=w/2;                            //屏幕位置换下一个位置k
			if(list>128-w/2)                      //判断是否需要换页
			{
				page+=h/8;
				list=2;
			}
		}
		else                                    //不是字符就是汉字
		{
			oled_dis_hz(page,list,str,w,h);      //调用显示一个汉字函数
			str+=2;                              //换数组下一个元素（汉字大小占两个位）
			list+=h;                             //屏幕位置换下一个位置（汉字的w和h是一样的）
			if(list>128-w)                       //判断是否需要换页 
			{
				page+=h/8;
				list=2;
			}
		}
	}
}


/************************************************
*函数名    ：oled_dis_pic
*函数功能  ：显示一个图形
*函数参数  ：u8 page,u8 list,const u8 *pic,u8 w,u8 h
*函数返回值：无
*函数描述  ：
						 page:0~7
						 list:2~128
*************************************************/
void oled_dis_pic(u8 page,u8 list,const u8 *pic,u8 w,u8 h)
{
	u8 page_n;
	u8 i,j;
	if(h%8==0)
	{
		page_n=h/8;
	}
	else
	{
		page_n=h/8+1;
	}
	for(i=0;i<page_n;i++)
	{
		OLED_setstart(page+i,list);
		for(j=0;j<w;j++)
		{
			OLED_writeByte(pic[w*i+j],OLED_DAT);
		}
	}
}


