#include "mpu6050.h"
#include "iic.h"

#define MPU6050_ADDRESS 0xD0

void MPU6050_WriteReg(u8 RegAddress,u8 Data)
{
	IIC_Start();
	IIC_SendByte(MPU6050_ADDRESS);
	IIC_ReceiveAck();
	IIC_SendByte(RegAddress);
	IIC_ReceiveAck();
	IIC_SendByte(Data);
	IIC_ReceiveAck();
	IIC_Stop();
}

uint8_t MPU6050_ReadReg(uint8_t RegAddress)
{
	uint8_t Data;
	
	IIC_Start();
	IIC_SendByte(MPU6050_ADDRESS);
	IIC_ReceiveAck();
	IIC_SendByte(RegAddress);
	IIC_ReceiveAck();
	
	IIC_Start();
	IIC_SendByte(MPU6050_ADDRESS | 0x01);
	IIC_ReceiveAck();
	Data = IIC_ReceiveByte();
	IIC_SendAck(1);
	IIC_Stop();
	
	return Data;
}

void MPU6050_Init(void)
{
	IIC_IO_Init();
	MPU6050_WriteReg(MPU6050_PWR_MGMT_1, 0x01);
	MPU6050_WriteReg(MPU6050_PWR_MGMT_2, 0x00);
	MPU6050_WriteReg(MPU6050_SMPLRT_DIV, 0x09);
	MPU6050_WriteReg(MPU6050_CONFIG, 0x06);
	MPU6050_WriteReg(MPU6050_GYRO_CONFIG, 0x18);
	MPU6050_WriteReg(MPU6050_ACCEL_CONFIG, 0x18);
}

uint8_t MPU6050_GetID(void)
{
	return MPU6050_ReadReg(MPU6050_WHO_AM_I);
}

mpuData MPU6050_GetData(void)
{
	mpuData data;
	uint8_t DataH, DataL;
	
	DataH = MPU6050_ReadReg(MPU6050_ACCEL_XOUT_H);
	DataL = MPU6050_ReadReg(MPU6050_ACCEL_XOUT_L);
	data.AccX = (DataH << 8) | DataL;
	
	DataH = MPU6050_ReadReg(MPU6050_ACCEL_YOUT_H);
	DataL = MPU6050_ReadReg(MPU6050_ACCEL_YOUT_L);
	data.AccY = (DataH << 8) | DataL;
	
	DataH = MPU6050_ReadReg(MPU6050_ACCEL_ZOUT_H);
	DataL = MPU6050_ReadReg(MPU6050_ACCEL_ZOUT_L);
	data.AccZ = (DataH << 8) | DataL;
	
	DataH = MPU6050_ReadReg(MPU6050_GYRO_XOUT_H);
	DataL = MPU6050_ReadReg(MPU6050_GYRO_XOUT_L);
	data.GyroX = (DataH << 8) | DataL;
	
	DataH = MPU6050_ReadReg(MPU6050_GYRO_YOUT_H);
	DataL = MPU6050_ReadReg(MPU6050_GYRO_YOUT_L);
	data.GyroY = (DataH << 8) | DataL;
	
	DataH = MPU6050_ReadReg(MPU6050_GYRO_ZOUT_H);
	DataL = MPU6050_ReadReg(MPU6050_GYRO_ZOUT_L);
	data.GyroZ = (DataH << 8) | DataL;
	
	return data;
}


// 步数计算算法 - 峰值检测
#define THRESHOLD 800 // 加速度阈值
int StepCounter(int16_t x, int16_t y, int16_t z) 
{
static int16_t prevZ = 0;
static int stepCount = 0;

int16_t mag = x * x + y * y + z * z;
int16_t deltaZ = z - prevZ;

if((mag > THRESHOLD) && (deltaZ > THRESHOLD)) 
	{
stepCount++;
}

prevZ = z;

return stepCount;
}





