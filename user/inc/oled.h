#ifndef _OLED_H
#define _OLED_H

#include "stm32f4xx.h"



//宏定义
//时钟线
#define OLED_SPI_SCL_H (GPIO_SetBits(GPIOB, GPIO_Pin_3))   //输出高电平
#define OLED_SPI_SCL_L (GPIO_ResetBits(GPIOB, GPIO_Pin_3))  //输出低电平

//数据线
#define OLED_MOSI_H (GPIO_SetBits(GPIOB, GPIO_Pin_5))		//输出高电平
#define OLED_MOSI_L (GPIO_ResetBits(GPIOB, GPIO_Pin_5))	//输出低电平

//复位线
#define OLED_RST_H			(GPIO_SetBits(GPIOB, GPIO_Pin_13))		//输出高电平
#define OLED_RST_L			(GPIO_ResetBits(GPIOB, GPIO_Pin_13))	//输出低电平

//片选
#define OLED_CS_H			(GPIO_SetBits(GPIOB, GPIO_Pin_7))		//关闭通讯
#define OLED_CS_L			(GPIO_ResetBits(GPIOB, GPIO_Pin_7))	//打开通讯

//数据/命令选择线
#define OLED_CD_H			(GPIO_SetBits(GPIOA, GPIO_Pin_15))		//选择数据
#define OLED_CD_L				(GPIO_ResetBits(GPIOA, GPIO_Pin_15))	//选择命令

//数据/命令宏定义
#define OLED_CMD 0
#define OLED_DAT 1



//函数声明
void spi1_init(void);                         //SPI1初始化函数
u8 spi1_byte(u8 data);                        //SPI1传输数据函数
void OLED_IO_init(void);                      //OLED所用IO初始化函数
void OLED_RST(void);                          //复位函数
void OLED_writeByte(u8 data,u8 cmd_data);     //主芯片发送数据/命令到OLED
void OLED_init(void);                         //OLED初始化函数
void OLED_clear(void);                        //清屏函数
void OLED_setstart(u8 page,u8 list);          //确定初始位置函数
void oled_dis_char16(u8 page,u8 list,u8 ch);  //显示一个16x16的字符函数
void oled_dis_char24(u8 page,u8 list,u8 ch);
void oled_dis_char(u8 page,u8 list,u8 ch,u8 w,u8 h);
void oled_dis_hz(u8 page,u8 list,u8 *hz,u8 w,u8 h);

void oled_dis_str(u8 page,u8 list,u8 *str,u8 w,u8 h);
void oled_dis_hz_str(u8 page,u8 list,u8 *hz,u8 w,u8 h);

void oled_dis(u8 page,u8 list,u8 *str,u8 w,u8 h);

void oled_dis_pic(u8 page,u8 list,const u8 *pic,u8 w,u8 h);

#endif 


