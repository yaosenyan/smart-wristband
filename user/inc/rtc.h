#ifndef _RTC_H
#define _RTC_H

#include "stm32f4xx.h"


typedef struct rtc
{
	u16 year;    //年份
	u8  mon;     //月份
	u8  day;     //天数
	u8  week;    //星期
	u8  h;       //小时
	u8  m;       //分钟
	u8  s;       //秒数
}RTC_t;

//函数声明
void RTC_init(RTC_t time);

ErrorStatus set_time(RTC_t time);
RTC_t get_time(void);

extern RTC_t val;



#endif



