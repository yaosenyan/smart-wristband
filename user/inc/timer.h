#ifndef _TIMER_H
#define _TIMER_H

#include "stm32f4xx.h"

void Tim11_Delay_Ms(u32 ms);
void Tim11_Delay_Us(u32 us);
void Timer9_Interrupt_Ms(u16 ms);
void TIM2_CH3_PWM_Init(void);
void PWM_SetComparel(u32 Compare);

#endif


