#ifndef _IIC_H
#define _IIC_H

#include "stm32f4xx.h"

//宏定义
//#define SHT20_W_SCL(x)  GPIO_WriteBit(GPIOB,GPIO_pin_8,(BitAction)(x))
//#define SHT20_W_SDA(x)  GPIO_WriteBit(GPIOB,GPIO_pin_9,(BitAction)(x))

//#define HP6_IIC_SCL_L	GPIO_ResetBits(GPIOA,GPIO_Pin_2)
//#define HP6_IIC_SCL_H	GPIO_SetBits(GPIOA,GPIO_Pin_2)

//#define HP6_IIC_SDA_OUT_L GPIO_ResetBits(GPIOA,GPIO_Pin_2)
//#define HP6_IIC_SDA_OUT_H 	GPIO_SetBits(GPIOA,GPIO_Pin_2)

////读数据线
//#define HP6_IIC_SDA_IN GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_1)

//函数声明
void IIC_W_SCL(u8 BitValue);
void IIC_W_SDA(u8 BitValue);
u8 IIC_R_SDA(void);
void IIC_Start(void);
void IIC_Stop(void);
void IIC_SendByte(u8 Byte);
u8 IIC_ReceiveByte(void);
void IIC_SendAck(u8 AckBit);
u8 IIC_ReceiveAck(void);
void IIC_IO_Init(void);

//HP6
void HP6_IIC_Star(void);
void HP6_IIC_Stop(void);
void HP6_IIC_SendAck(u8 AckBit);
u8 HP6_IIC_ReceiveAck(void) ;
void HP6_IIC_SendByte(u8 Byte);
u8 HP6_IIC_ReceiveByte(void);
void HP6_IIC_IO_Init(void);

#endif

