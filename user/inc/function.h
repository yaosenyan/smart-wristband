#ifndef _FUNCTION_H
#define _FUNCTION_H

#include "stm32f4xx.h" 
//宏定义 --- 别加分号


//函数声明 --- 必须加分号
void start_k(void);
void key_page(void);
void function_page1(void);
void function_page2(void);
void function_page3(void);
void function_page4(void);
void function_page5(void);
void function_page6(void);

//外部声明
extern u8 a_flag;
extern u8 page;
extern char a[4][10];

#endif



