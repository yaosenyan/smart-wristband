#ifndef _SHT20_H
#define _SHT20_H

#include "stm32f4xx.h"

typedef union trh     //共用体
{
	float t;     //温度成员
	u8 rh;       //湿度成员
}TRH_n;

//宏定义
#define SHT20_ADDR_W 0x80            //写器件地址
#define SHT20_ADDR_R 0x81            //读器件地址

#define SHT20_SOFT 0xfe
#define SHT20_T 0xf3
#define SHT20_RH 0xf5

//函数声明
void SHT20_Init(void);
void SHT20_Soft(void);
TRH_n SHT20_REC_VAL(u8 cmd);


#endif


