#ifndef _HP6_H
#define _HP6_H
//宏定义和函数声明

#include "main.h" 




//输入
#define HP6_IIC_R_SDA GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_1)

//器件地址
#define Hp6_ADDR_W (0x66<<1)
#define Hp6_ADDR_R (0x66<<1 | 0x01)

//供电引脚宏定义
#define  Hp_6_PowerON (GPIOC->ODR &= ~(1 << 13))
#define  Hp_6_PowerOFF (GPIOC->ODR |= (1 << 13))



//iic
void hp6_iic_io_init(void);      //IIC初始化函数
void hp6_iic_star(void);         //IIC开始信号函数
void hp6_iic_stop(void);         //IIC停止信号函数
void hp6_iic_send_ack(u8 ack);   //IIC发送应答/不应答函数
u8 hp6_iic_get_ack(void);        //IIC返回应答/不应答函数
void hp6_iic_send_byte(u8 data); //IIC发送一个字节函数
u8 hp6_iic_get_byte(void);       //IIC接收一个字节函数

//hp6
void Hp_6_I2CWrite(u8 *pData);
void Hp_6_init(void);
//心率
u8 HP_6_OpenRate(void);
u8 HP_6_CloseRate(void);
u8 HP_6_GetRateResult(u8 *result);
//血压
u8 HP_6_Openbp(void);
u8 HP_6_Closebp(void);
u8 HP_6_Get_bpResult(u8 *r_result);
#endif



